import yaml
import subprocess
import registry as Registry
import harbor as Harbor_module
import relation as Relation_module
import os
import logging
import re
import json


def logandprint(logger, message):
    print(message)
    logger.debug(message)


def dockerlogin(host, username, password, logger):
    cmd = "docker login --username={} --password={} {}".format(
        username, password, host
    )
    logandprint(logger, cmd)
    process = subprocess.Popen(
        cmd, shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True, preexec_fn=os.setsid)

    while process and process.poll() is None:
        output = process.stdout.readline().strip()
        if output:
            logandprint(logger, output)
    print("\n")


def getconfiginfo(logger):
    logandprint(logger, "GetConfigInfo...")
    with open("migrate.yaml", 'r') as f:
        data = yaml.load(f,Loader=yaml.FullLoader)
    return data


def getharborservices(harbors, logger):
    logandprint(logger, "Get Habror info from userinput...")
    harborservices = {}
    for harbor in harbors:
        newharbor = Harbor_module.Harbor(harbor["name"], harbor["endpoint"], harbor["username"],
                                         harbor["password"], harbor["registryhost"])
        harborservices[harbor["name"]] = newharbor
    return harborservices


def getrelationinstances(relations, harborservices, registrytomigrate):
    relationsinstances = []
    for relation in relations:
        if "projectname" in relation.keys():
            projectname = relation["projectname"]
        else:
            projectname = ""
        newrelation = Relation_module.Relation(registrytomigrate[relation["alauda"]],
                                               harborservices[relation["harbor"]], relation["type"], projectname)
        relationsinstances.append(newrelation)
    return relationsinstances


def getharborprojectnametorelations(relationsinstances, rooturl, header, logger):
    # 把project映射到harbor的名字给对应出来
    for relationinstance in relationsinstances:
        registry = relationinstance.registry
        projects = registry.getprojects(rooturl, header)

        if relationinstance.type == "equal":
            relationinstance.projects.append(Registry.Project("shared", registry.displayname + "-shared"))
            for project in projects:
                relationinstance.projects.append(Registry.Project(project, project))

        elif relationinstance.type == "shared":
            relationinstance.projects.append(Registry.Project("shared", registry.displayname + "-shared"))
            for project in projects:
                relationinstance.projects.append(Registry.Project(project, registry.displayname + "-" + project))

        elif relationinstance.type == "public":
            relationinstance.projects.append(Registry.Project("shared", relationinstance.projectname))

        else:
            logandprint(logger, "no such type {} for relation type".format(relationinstance.type))


def filterprojectandrepo(relationsinstances, AlaudaAdministor, logger):
    logandprint(logger, "Filter repo for migrating...")
    for relationinstance in relationsinstances:
        patterns = []
        for filter in relationinstance.registry.filters:
            compileuse = r'{}'.format(filter)
            pattern = re.compile(compileuse)
            patterns.append(pattern)

        if relationinstance.registry.ispublic == True:
            alaudaroot = "{}/".format(AlaudaAdministor)
        else:
            alaudaroot = ""

        for project in relationinstance.projects[:]:
            for repository in project.repositories[:]:
                if repository.ispublic:
                    repository.imageaddr = "{}/{}{}".format(relationinstance.registry.endpoint, alaudaroot,
                                                            repository.name)
                else:
                    repository.imageaddr = "{}/{}{}/{}".format(relationinstance.registry.endpoint, alaudaroot,
                                                               project.name, repository.name)
                if shoulddeleterepository(patterns, repository.imageaddr):
                    project.repositories.remove(repository)

            if len(project.repositories) == 0:
                relationinstance.projects.remove(project)


def shoulddeleterepository(patterns, repositoryName):
    for pattern in patterns:
        if pattern.match(repositoryName) != None:
            return False
    return True


def warninguser(relationsinstances, logger):
    logandprint(logger, "Tips: shared 代表每个registry中的共享空间，如果registry为公有registry, 则shared代表公有registry \n")

    for relationinstance in relationsinstances:
        for project in relationinstance.projects:
            logandprint(logger,
                        "平台上的 {} 镜像源下的 {} 项目中的所有镜像仓库都会被迁移到Harbor, 并且所有迁移过去的镜像仓库会在 Harbor 的 {} 项目中, 如有疑问, 请查看 README.md 中的内容".format(
                            relationinstance.registry.displayname,
                            project.name,
                            project.harborname))
            logandprint(logger, "请检查将要迁移过去的镜像仓库，是否符合预期:")
            for repository in project.repositories:
                logandprint(logger, repository.name)

    TODO = input("请确认以上镜像迁移信息符合您的预期  如果同意, 请键入 yes , 否则, 请键入其他任意键 \n")
    if TODO != "yes":
        logandprint(logger, "您的输入不是 yes, 程序将会退出")
        os._exit(0)


def getrepoforrelations(relationsinstances, rooturl, header):
    for relationsinstance in relationsinstances:

        publicarepositoriesname = relationsinstance.registry.getpublicrepositories(rooturl, header)

        for publicrepositoryname in publicarepositoriesname:
            prepository = Registry.Repository(publicrepositoryname, True)
            for project in relationsinstance.projects:
                if project.name == "shared":
                    project.repositories.append(prepository)

        for project in relationsinstance.projects:
            repoistories = relationsinstance.registry.getprojectrepositories(rooturl, project.name, header)
            for repo in repoistories:
                repository = Registry.Repository(repo, False)
                project.repositories.append(repository)


def gettagsforallrepo(relationsinstances, rooturl, header, logger):
    logandprint(logger, "获取tag信息, 如果同步的镜像比较多, 这可能会用上几分钟...")
    tasknum = 0
    for relationsinstance in relationsinstances:
        for project in relationsinstance.projects:
            for repo in project.repositories:
                if repo.ispublic:
                    projectname = ""
                else:
                    projectname = project.name

                tags = repo.gettag(rooturl, relationsinstance.registry.name, projectname, repo.name, header)
                tagnum = len(tags)
                tasknum += tagnum
                repo.tags = tags
    return tasknum


def migrate(relationsinstances, AlaudaAdministor, tasknum, AlaudaUserName, AlaudaPassword, logger):
    # 循环 relation 里面的repositroy  然后再进行同步
    totaltasknum = tasknum
    failed = 0
    failediamgetag = []
    registryrelations = {}
    for relationsinstance in relationsinstances:
        relationsinstance.harbor.login(logger)
        dockerlogin(relationsinstance.registry.endpoint, AlaudaUserName, AlaudaPassword, logger)
        for project in relationsinstance.projects:

            httpstauscode, response = relationsinstance.harbor.createproject(project.harborname)

            if httpstauscode == 201:
                harborresponse = "Create Harbor project {} succeed  httpstatuscode is {} ".format(
                    project.harborname, httpstauscode)
                logandprint(logger, harborresponse)
            elif httpstauscode == 409:
                harborresponse = "project {} exist, will use it".format(
                    project.harborname)
                logandprint(logger, harborresponse)
            else:
                harborresponse = "Error Happend when create Harbor project {}, httpstatuscode is {}, error is {},will skip this project".format(
                    project.harborname, response, httpstauscode)
                logandprint(logger, harborresponse)
                continue

            for repository in project.repositories:
                alaudaimagewithouttag = repository.imageaddr
                harborimagewithouttag = "{}/{}/{}".format(relationsinstance.harbor.registryhost, project.harborname,
                                                          repository.name)
                registryrelations[alaudaimagewithouttag] = harborimagewithouttag
                for tag in repository.tags:
                    tasknum, failed, success = migrateimage(alaudaimagewithouttag, harborimagewithouttag, tag, logger,
                                                            tasknum,
                                                            totaltasknum, failed)
                    if not success:
                        failediamgetag.append("{}:{}".format(alaudaimagewithouttag,tag))

                deletecmd = "docker rmi -f $(docker image ls | grep '{}' | awk '{}')".format(
                    alaudaimagewithouttag, "{print $3}")
                logandprint(logger, deletecmd)
                execute_cmd(deletecmd, logger)
    return failediamgetag, registryrelations


def migrateimage(alaudaimagewithouttag, harborimagewithouttag, tag, logger, tasknum, totaltasknum, failed):
    alaudaimage = "{}:{}".format(alaudaimagewithouttag, tag)
    harborimage = "{}:{}".format(harborimagewithouttag, tag)
    success = False
    for time in range(3):
        pullcmd = "docker pull {}".format(
            alaudaimage
        )
        logandprint(logger, pullcmd)
        returncode = execute_cmd(pullcmd, logger)
        if returncode != 0:
            logandprint(logger, "{} can't be done, try again...".format(pullcmd))
            continue

        tagcmd = "docker tag {} {}".format(
            alaudaimage, harborimage
        )
        logandprint(logger, tagcmd)
        returncode = execute_cmd(tagcmd, logger)
        if returncode != 0:
            logandprint(logger, "{} can't be done, try again...".format(tagcmd))
            continue

        pushcmd = "docker push {}".format(
            harborimage
        )
        logandprint(logger, pushcmd)

        returncode = execute_cmd(pushcmd, logger)
        if returncode != 0:
            logandprint(logger, "{} can't be done, try again...".format(pushcmd))
            continue
        success = True
        break

    if not success:
        failed += 1
    tasknum -= 1

    logandprint(logger, "total {}, {} done, {} failed, {} left... \n".format(totaltasknum,
                                                                             totaltasknum - tasknum, failed,
                                                                             tasknum))
    return tasknum, failed, success


def execute_cmd(cmd, logger):
    process = subprocess.Popen(
        cmd, shell=True,
        stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True, preexec_fn=os.setsid)

    while process and process.poll() is None:
        output = process.stdout.readline().strip()
        if output:
            logandprint(logger, output)
    return process.returncode


def Process():
    logger = logging.getLogger("logger")
    filehandle = logging.FileHandler(filename="migrate.log", mode='w')
    logger.addHandler(filehandle)
    logger.setLevel(logging.DEBUG)

    data = getconfiginfo(logger)
    AlaudaEndpoint = data["alauda"]["endpoint"]
    AlaudaAdministor = data["alauda"]["rootaccount"]
    AlaudaToken = data["alauda"]["token"]

    AlaudaUserName = data["alauda"]["username"]
    AlaudaPassword = data["alauda"]["password"]

    harbors = data["harbor"]
    relations = data["relation"]
    alaudaregistories = data["alauda"]["registries"]

    Alauda = Registry.Alauda(AlaudaEndpoint, AlaudaAdministor, AlaudaToken, AlaudaUserName,
                             AlaudaPassword)
    registries = Alauda.getregistries(logger)
    rooturl = Alauda.getrooturl()
    header = Alauda.getheader()

    registriesnames = []
    registryfilters = {}
    for registry in alaudaregistories:
        registriesnames.append(registry["displayname"])
        registryfilters[registry["displayname"]] = registry["filters"]

    registrytomigrate = Alauda.getregistrytomigrate(registriesnames, registryfilters, registries)

    harborservices = getharborservices(harbors, logger)

    relationsinstances = getrelationinstances(relations, harborservices, registrytomigrate)

    getharborprojectnametorelations(relationsinstances, rooturl, header, logger)

    getrepoforrelations(relationsinstances, rooturl, header)

    filterprojectandrepo(relationsinstances, AlaudaAdministor, logger)

    warninguser(relationsinstances, logger)

    tasknum = gettagsforallrepo(relationsinstances, rooturl, header, logger)

    failediamgetag, registryrelations = migrate(relationsinstances, AlaudaAdministor, tasknum, AlaudaUserName, AlaudaPassword, logger)

    try:
        relationjson = json.dumps(registryrelations, indent=4)
        file = open("registryrelation.json", "a+")
        file.write(relationjson)
        file.close()
    except Exception as err:
        logandprint(logger, "write to file registryrelations Failed and err is {}".format(err))

    if len(failediamgetag) != 0:
        logandprint(logger, "以下镜像同步失败, 请检查日志并确认")
        for failediamge in failediamgetag:
            logandprint(logger, "FAILED: {}".format(failediamge))

    logandprint(logger, "镜像同步工作已经完成，请检查同步结果是否符合预期结果")


if __name__ == "__main__":
    Process()
