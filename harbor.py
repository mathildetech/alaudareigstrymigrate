import requests
import json
import subprocess
import os
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class Harbor(object):
    def __init__(self, servicename, endpoint, username, password, registryhost):
        self.endpoint = endpoint
        self.username = username
        self.password = password
        self.servicename = servicename
        self.registryhost = registryhost

    def createproject(self, projectname):
        url = "{}/api/projects".format(self.endpoint)
        payload = {
            'project_name': projectname,
            "metadata": {
                "public": "true"
            }
        }
        headers = {'content-type': 'application/json'}
        response = requests.post(url, data=json.dumps(payload), headers=headers,
                                 auth=requests.auth.HTTPBasicAuth(self.username, self.password), verify=False)
        return response.status_code, response.text

    def login(self, logger):
        cmd = "docker login --username={} --password={} {}".format(
            self.username, self.password, self.endpoint
        )
        process = subprocess.Popen(
            cmd, shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True, preexec_fn=os.setsid)

        while process and process.poll() is None:
            output = process.stdout.readline().strip()
            if output:
                logger.debug(output)
