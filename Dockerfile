FROM docker
RUN apk --update add --no-cache \
        python3 \
		py3-pip
RUN mkdir /app
RUN python3 -m pip install --upgrade pip
RUN pip install pyyaml
RUN pip install requests
WORKDIR /app
COPY bootstrap.sh harbor.py process.py registry.py relation.py README.md /app/
ENTRYPOINT ["./bootstrap.sh"]