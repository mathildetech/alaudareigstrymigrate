import requests
import json
import process
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class Alauda(object):
    def __init__(self, endpoint, rootaccount, token, username, password):
        self.endpoint = endpoint
        self.rootaccount = rootaccount
        self.token = token
        self.username = username
        self.password = password

    def getrooturl(self):
        return "{}/v1/registries/{}/".format(self.endpoint, self.rootaccount)

    def getheader(self):
        headers = {'Authorization': 'token ' + self.token}
        return headers

    def getregistries(self, logger):
        process.logandprint(logger, "Get registry info from platform...")
        getregistryurl = self.getrooturl()
        headers = self.getheader()

        response = requests.get(getregistryurl, headers=headers, verify=False)
        reponsetext = json.loads(response.text)
        registries = []
        for registry in reponsetext:
            newregistry = Registry(registry["name"], registry["display_name"], registry["endpoint"],
                                   registry["is_public"], [])
            registries.append(newregistry)
        return registries

    def getregistrytomigrate(self, registriesnames, registryfilters, registies):
        registriestomigrate = {}
        for registry in registies:
            if registry.displayname in registriesnames:
                registry.filters = registryfilters[registry.displayname]
                registriestomigrate[registry.displayname] = registry
        return registriestomigrate


class Registry(object):

    def __init__(self, name, displayname, endpoint, ispublic, filters):
        self.name = name
        self.endpoint = endpoint
        self.displayname = displayname
        self.ispublic = ispublic
        self.filters = filters

    def getprojects(self, rooturl, headers):
        getprojecturl = "{}{}/projects".format(rooturl, self.name)
        response = requests.get(getprojecturl, headers=headers, verify=False)
        reponsetext = json.loads(response.text)
        projects = []
        for project in reponsetext:
            projects.append(project["project_name"])
        return projects

    def getpublicrepositories(self, rooturl, headers):
        getpublicrepositoriesurl = "{}{}/repositories".format(rooturl, self.name)
        response = requests.get(getpublicrepositoriesurl, headers=headers, verify=False)
        reponsetext = json.loads(response.text)
        repositories = []
        for repository in reponsetext:
            repositories.append(repository["name"])
        return repositories

    def getprojectrepositories(self, rooturl, projectname, headers):
        getprojectrepositoriesurl = "{}{}/projects/{}/repositories".format(rooturl, self.name, projectname)
        response = requests.get(getprojectrepositoriesurl, headers=headers, verify=False)
        reponsetext = json.loads(response.text)
        repositories = []
        for repository in reponsetext:
            repositories.append(repository["name"])
        return repositories


class Project(object):
    def __init__(self, name, harborname):
        self.name = name
        self.harborname = harborname
        self.repositories = []


class Repository(object):

    def __init__(self, name, ispublic):
        self.name = name
        self.ispublic = ispublic
        self.imageaddr = ""
        self.tags = []


    def gettag(self, rooturl, registryname, projectname, repositoryname, headers):
        if projectname == "":
            gettagsurl = "{}{}/repositories/{}/tags".format(rooturl, registryname, repositoryname)
        else:
            gettagsurl = "{}{}/projects/{}/repositories/{}/tags".format(rooturl, registryname, projectname,
                                                                        repositoryname)
        response = requests.get(gettagsurl, headers=headers, verify=False)
        reponsetext = json.loads(response.text)
        return reponsetext
