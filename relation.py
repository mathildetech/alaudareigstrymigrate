class Relation(object):
    def __init__(self, registry, harbor, type, projectname):
        self.registry = registry
        self.harbor = harbor
        self.type = type
        # only useful for public registry
        self.projectname = projectname
        self.projects = []
