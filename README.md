### 使用方法

在[镜像迁移步骤](http://confluence.alaudatech.com/pages/viewpage.action?pageId=39337975) 文档中有详细说明, 改文档不再进行维护


* 阅读注意事项
* 按照文档内容创建 migrate.yaml 文件
* 拉取镜像 index.alauda.cn/alaudaorg/alaudaregistrymigrate:latest
* 运行该镜像 docker run  -v ABSOLUTE/PATH/migrate.yaml:/app/migrate.yaml -v /var/run/docker.sock:/var/run/docker.sock -d index.alauda.cn/alaudaorg/alaudaregistrymigrate:latest
* 执行 docker ps|grep migrate 找到启动的容器， 执行 docker exec -it CONTANIERID sh 
* 执行 python3 process.py, 然后按照提示进行输入, 等待同步结束。
* 同步过程中产生的日志会放在 migrate.log 文件中, 同步有问题的话, 可以通过日志进行排查


### migrate.yaml 文件参数说明

migrate.yaml

```
alauda:
 endpoint: # 平台api url  用于获取平台镜像相关信息
 rootaccount: # 根账号
 token: # 根账号Token
 username: # 登录镜像仓库源地址的用户名 例如: alaudaorg/zpyu
 password: # 登录镜像仓库源地址的密码
 registries:
 - displayname: Alauda # 想要同步的镜像源的展示名称
   filters: # 过滤条件 可以为数组 支持正则 如果为多个过滤条件 则会对多个条件进行匹配 有一个条件匹配 就会被同步  例如 如下过滤条件 则在该镜像源中 project或者repoame中包含enigma的镜像仓库 都会被同步, 项目project下的enigma会被同步过去
    - ".*enigma.*"
    - ".*project/enigma.*"
harbor:
- name: harbor1  # Harbor名称
  endpoint: http://harbor.devsparrow.host # Harbor服务地址
  registryhost: harbor.devsparrow.host # Harbor镜像仓库源地址
  username: admin # Harbor 服务Admin账号
  password: xxxxxxx # Habror 服务Admin密码
relation:
- alauda: Alauda # 想要同步的镜像源的展示名称
  harbor: harbor1 # Habror名称
  type: public # 同步类型
  projectname: publicreigstry # 如果同步类型为public 则该参数为必选项 否则不需要该参数
```
### relation type 说明
relation 中的 type 的可选值为 public, shared , equal, 他们的不同如下所示, 主要影响的是, 在平台上的镜像仓库项目, 同步到 Harbor 后, 会用什么样的名称

#### public
* 使用场景: 如果同步的 registry 是公有镜像源, 则需要将类型设置为 public, 并且设置 projectname
* 效果: 平台上的公有镜像源的所有镜像, 都会被同步到 Harbor 上名字为 projectname 的项目下

#### equal:
* 使用场景: 如果同步的registry不是公有镜像源, 并且在所有的 relation 中, 平台镜像源与 Harbor 服务是一一对应的,即不存在多个镜像仓库源同步到一个 Harbor 的情况
* 效果: 该镜像仓库源共享空间中的镜像仓库,将会被同步到Harbor中名称为 registryName + shared 的项目中，该镜像仓库源下其他的project下的镜像仓库, 将会同步到Harbor下以project名称为项目名称的项目中

#### shared:
* 使用场景: 如果同步的 registry 不是公有镜像源, 并且在所有的 relation 中, 平台镜像源与 Harbor 服务不是一一对应的,即存在多个镜像仓库源同步到一个 Harbor 的情况
* 效果: 该镜像仓库源下共享空间中的镜像仓库将会被同步到 Harbor 中名字为 registryName + shared 的项目中，该镜像仓库源下其他的 project 下的镜像仓库, 将会同步到 Harbor 下以 registryName + project 名称为项目名称的项目中

### 注意事项
* 如果是很大的registry的迁移 比如公有 registry 的迁移  建议一次只迁移一个 registry
* 为节约同步时间，可以先将平台上的不需要的镜像先删除 再进行迁移
* 保证执行该脚本的主机可以连接到 Harbor 和 平台
* 同步结束后，同步到 Harbor 上的 project 的都是公开的访问规则，用户可以根据自身需求进行调整